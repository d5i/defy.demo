# defy.tools Node Demo
Console application demonstration for `defy.tools` - Tools for developing Private Channels [Nostr](https://github.com/fiatjaf/nostr) clients.

## Demo Instructions
- `git clone git@gitlab.com:d5i/defy.demo.git`
- `cd defy.demo`
- `npm install`
- `ts-node src/node/index.ts`

## License
MIT (c) 2023 David Krause [https://defy.social](https://defy.social), see LICENSE file.

