import { bytesToHex, randomBytes } from '@noble/hashes/utils';
import chalk from 'chalk';
import { ContactChannel, KeyStore, KeyStoreFileStorage, KeyStoreStorage, uniqueNotesBySenderAndDate, ProfileMetadata, RelayPool, Notification } from "defy.tools";
import { bufferTime, filter, mergeMap, of, tap, switchMap } from "rxjs";
import { MessageDemoOptions, showDebugDetails } from './options';

// This is a very simple demo app that runs in a terminal.   In a more robust application, most of these option values
// would be read from somewhere else.
const options: MessageDemoOptions = {
    // Leave privateKey blank to create a new profile, or enter an nsec to use that profile as the sender
    privateKey: '',
    // KeyStoreFileStorage stores your key encrypted with this password in a readonly file at $HOME/.defy/defy-backup
    password: 'Demo Password!',
    // The pubkeys to message
    participants: [
        '828adc3061e4f0c3f1984dce96003eb89d2ab279e703e01de806c9b2ba33ff73',
        'f6970e087ed72da1c8f645b0d5613494194bd0a7c35e77382f821bb49e7c8096'
    ],
    // This profile will be we saved if the app creates a new profile/key, not if you have one already
    profile: {
        name: 'DemoProfile',
        display_name: 'Profile ' + bytesToHex(randomBytes(4)),
        picture: 'https://cdn-icons-png.flaticon.com/256/3917/3917688.png'
    } as ProfileMetadata,
    // Set to true to see more verbose message details on each message
    showDebugDetails: false
};

let keyStore: KeyStore;
const relayPool = new RelayPool();
relayPool.addRelay('wss://nostr.mom', true, true, false);

(async function () {

    // 1) Get the keyStore
    if (options.privateKey) {
        keyStore = await KeyStoreStorage.fromPrivateKey(KeyStoreFileStorage, { privateKey: options.privateKey });
    } else if (KeyStoreFileStorage.hasBackupKey) {
        keyStore = await KeyStoreStorage.fromStorage(KeyStoreFileStorage, options.password);
    } else {
        keyStore = KeyStoreStorage.create(KeyStoreFileStorage);
    }

    // 2) Save the key information for the next time you run this app
    await keyStore.save(options.password);

    // 3) Set the defyContext.  defy.tools requires a keyStore and a relayPool object in the defyContext
    setDefyContext({ keyStore, relayPool });

    // 4) Save the new profile so that others you message with can easily identify you
    if (keyStore.isNew && options.profile) {
        relayPool.savePublicProfile(options.profile).subscribe();
    }

    // 5) Set the full participants list and a sessionInfo message
    options.participants.push(keyStore.ownerPubKey);
    let sessionInfo = chalk.green(`Session started between\n${options.participants.join(' \nand ')}.`) + '\nEnter a message:';

    // 6) Load the existing channels on this keyStore to find or create a channel
    keyStore.load().pipe(

        // 7) Find a channel with these participants or create a new one
        switchMap(() => {
            const channelId = ContactChannel.getChannelId(options.participants);
            let channel = keyStore.contacts.find(x => x.channelId === channelId);
            if (channel) {
                return of(channel);
            } else {
                return ContactChannel.create(options.participants, true);
            }
        }),

        // 8) Read stdin to send message text input typed in the console
        tap(channel => {
            const txtDecoder = new TextDecoder();
            process.openStdin().addListener("data", (input) => {
                const text = txtDecoder.decode(input).replace(/\n$/, '').trim();
                if (text.length) {
                    if (text.startsWith('!!')) {
                        channel.notify(text.substring(2)).subscribe();
                    } else {
                        channel.sendMessage(text).subscribe();
                    }
                }
            });
        }),

        // 9) Handle Notifications
        tap(channel => {
            // 9a) Listen for Notifications.  This handles notifications for all threads, not just the current one.
            keyStore.notifyIndex.listen()[0].subscribe();
            // 9b) Save new keys received in the channel content.  
            // This would be optional if senders never delete their notifications, 
            // but message delete ability is an important feature.
            channel.notifKey.pipe(
                filter(Boolean),
                filter(notif => notif.ownerPubKey !== keyStore.ownerPubKey),
                mergeMap(() => channel.publish())
            ).subscribe()
        }),

        // 10) Listen for notes on the channel.   channel.listen() returns both an observable of Note (offset 0)
        // and a method to end the listen (offset 1).   In this console application, we are rely on
        // Ctrl-C to end the session so we can ignore offset 1 of the listen() call.
        switchMap(channel => channel.listen()[0]),

        // 11) Wait for batches of Notes every 3 seconds
        bufferTime(3000)

    ).subscribe((messages) => {

        // 12) Filter out duplicate messages sent unless the showDebugDetails option is true. 
        // In groups with more than 2 participants, sent messages will appear as duplicates with the same text
        // and created_at values, but encrypted for a different index.  Turn showDebugDetails to true to see all
        // sent message copies.
        if (!options.showDebugDetails) {
            messages = messages.filter(uniqueNotesBySenderAndDate);
        }

        // 13) Sort and render each message
        messages
            .sort((a, b) => a.nostrEvent.created_at - b.nostrEvent.created_at)
            .forEach(note => {
                const isDocOwner = keyStore.ownerPubKey === note.ownerPubKey;
                const date = new Date(note.nostrEvent.created_at * 1000).toLocaleString().replace(', ', ' ');
                const from = isDocOwner ? "you" : note.ownerPubKey.substring(0, 8);
                console.log(`${chalk.yellow(date)} ${chalk.blue(from)}:\n\t${note.content.text}`);
                if (options.showDebugDetails) {
                    showDebugDetails(note, isDocOwner, options.participants);
                }
            })

        // 14) Render the sessionInfo message if there is one
        if (sessionInfo) {
            console.log(sessionInfo);
            sessionInfo = '';
        }
    });
})();


