import { bytesToHex } from "@noble/hashes/utils";
import chalk from "chalk";
import { HARDENED_KEY_OFFSET, HDKXNotification, HDKXTwoParty, Note, ProfileMetadata, v2 } from "defy.tools"

export interface MessageDemoOptions {
    /**
     * privateKey (optional) - leave it blank to create a new test profile, or enter an nsec and try it out
     */
    privateKey?: string,
    /**
     * password will be used to encrypt your keyStore data in the $HOME/.defy directory
     */
    password: string,
    /**
     * participants - an array of pubkeys to message with.  there is no limit in theory, 
     * but because each participant gets their own unique message event, relays might 
     * spam block a large number of messages sent at once 
     */
    participants: string[],
    /**
     * a profile is not needed to send and receive messages, but it helps others keep track
     * of who they are messaging.  This profile metadata is saved ONLY if we created a new key.
     */
    profile?: ProfileMetadata,
    /**
     * When showDebugDetails is true, the console will dosplay much more verbose output about
     * the message events and how their keys were derived.
     */
    showDebugDetails: boolean
};

/**
 * Re-calculates all the keys used to read the event and outputs key and event information
 * to the console solely for the purpose of demonstration and debugging event issues.
 */
export function showDebugDetails(note: Note, isDocOwner: boolean, participants: string[]) {
    console.debug(chalk.bgGray(`\tindex.type:`) + chalk.gray(` ${note.dkxParent.type}`));
    console.debug(chalk.bgGray(`\tevent.content:`) + chalk.gray(` ${note.nostrEvent.content}`));
    console.debug(chalk.bgGray(`\tevent.pubkey:`) + chalk.gray(` ${note.nostrEvent.pubkey}`));
    console.debug(chalk.bgGray(`\tevent.created_at:`) + chalk.gray(` ${note.nostrEvent.created_at}`));
    console.debug(chalk.bgGray(`\tevent.tags[0][1]:`) + chalk.gray(` ${note.nostrEvent.tags[0][1]}`));
    if (note.dkxParent instanceof HDKXTwoParty) {
        const senderOffset = (note.dkxParent as HDKXTwoParty).groupOffset || 0;
        const recverOffset = participants.sort().findIndex(x => x === note.ownerPubKey) || 0;
        if (isDocOwner) {
            console.debug(chalk.bgGray(`\tusing privateKey from sendKey/${HARDENED_KEY_OFFSET - note.nostrEvent.created_at}/${senderOffset} and publicKey from recvKey/${note.nostrEvent.created_at}/${recverOffset}`));
            console.debug(chalk.bgGray(`\tconversationKey:`) + chalk.gray(` ` + bytesToHex(v2.utils.getConversationKey(
                note.dkxParent.encryptParent.deriveChildKey(HARDENED_KEY_OFFSET - note.nostrEvent.created_at).deriveChildKey(senderOffset).hexPrivKey!,
                note.dkxParent.signingParent.deriveChildKey(note.nostrEvent.created_at).deriveChildKey(recverOffset).nostrPubKey
            ))));
        } else {
            console.debug(chalk.bgGray(`\tusing privateKey from recvKey/${note.nostrEvent.created_at}/${recverOffset} and publicKey from sendKey/${HARDENED_KEY_OFFSET - note.nostrEvent.created_at}/${senderOffset}`));
            console.debug(chalk.bgGray(`\tconversationKey: `) + chalk.gray(bytesToHex(v2.utils.getConversationKey(
                note.dkxParent.signingParent.deriveChildKey(note.nostrEvent.created_at).deriveChildKey(recverOffset).hexPrivKey!,
                note.dkxParent.encryptParent.deriveChildKey(HARDENED_KEY_OFFSET - note.nostrEvent.created_at).deriveChildKey(senderOffset).nostrPubKey
            ))));
        }
    }
    if (note.dkxParent instanceof HDKXNotification) {
        const groupOffset = (note.dkxParent as HDKXNotification).groupOffset || 0;
        if (isDocOwner) {
            console.debug(chalk.bgGray(`\tusing privateKey from sendKey/${note.nostrEvent.created_at}/${groupOffset} and publicKey from recvKey/${note.nostrEvent.created_at}`));
            console.debug(chalk.bgGray(`\tconversationKey: `) + chalk.gray(bytesToHex(v2.utils.getConversationKey(
                note.dkxParent.encryptParent.deriveChildKey(note.nostrEvent.created_at).deriveChildKey(groupOffset).hexPrivKey!,
                note.dkxParent.signingParent.deriveChildKey(note.nostrEvent.created_at).nostrPubKey
            ))));
        } else {
            console.debug(chalk.bgGray(`\tusing privateKey from recvKey/${note.nostrEvent.created_at} and publicKey from the event`));
            console.debug(chalk.bgGray(`\tconversationKey: `) + chalk.gray(bytesToHex(v2.utils.getConversationKey(
                note.dkxParent.signingParent.deriveChildKey(note.nostrEvent.created_at).hexPrivKey!,
                note.nostrEvent.pubkey
            ))));
        }
    }
}